#    This file is part of extra500
#
#    extra500 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    extra500 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with extra500.  If not, see <http://www.gnu.org/licenses/>.
#
#      Authors: Dirk Dittmann
#      Date: Jun 26 2013
#
#      Last change:      Eric van den Berg
#      Date:             05.12.2018
#

# MM Page 580, see system/flaps.xml

var FlapSystemClass = {
	new : func(root,name){
		var m = {parents:[
			FlapSystemClass,
			ServiceClass.new(root,name)
		]};
		
		m._switch 	= SwitchFactory.new("extra500/system/flap/Switch","Flap Switch",SwitchFactory.config("INT",-1,1,1,1,{"30":-1,"15":0,"0":1}));
	 	m._switch.onStateChange = func(n){
			me._state	= n.getValue();
		};
		
		m._ledTrans = LedClass.new("/extra500/light/FlapTransition","Flap Led Trans","extra500/system/dimming/Annunciator",0.6);
		m._led15 = LedClass.new("/extra500/light/Flap15","Flap Led 15","extra500/system/dimming/Annunciator",0.6);
		m._led30 = LedClass.new("/extra500/light/Flap30","Flap Led 30","extra500/system/dimming/Annunciator",0.6);
		

		return m;
	},
	setListeners : func(instance) {
		append(me._listeners, setlistener("/systems/flaps/transitionlight",func(n){me._updateTransitionLight(n);},1,0) );
		append(me._listeners, setlistener("/systems/flaps/light15deg",func(n){me._update15degLight(n);},1,0) );
		append(me._listeners, setlistener("/systems/flaps/light30deg",func(n){me._update30degLight(n);},1,0) );
		append(me._listeners, setlistener(extra500.dimmingSystem._nTest,func(n){instance._onDimTestChange(n);},1,0) );
		append(me._listeners, setlistener("/systems/flaps/inbalance/state",func(n){me._updateUNBCB(n);},1,0) );

	},
	init : func(instance=nil){
		if (instance==nil){instance=me;}
		me.parents[1].init(instance);
		me.setListeners(instance);
				
		me._switch.init();
		me._ledTrans.init();
		me._led15.init();
		me._led30.init();
				
		eSystem.circuitBreaker.WARN_LT.outputAdd(me._ledTrans);
		eSystem.circuitBreaker.WARN_LT.outputAdd(me._led15);
		eSystem.circuitBreaker.WARN_LT.outputAdd(me._led30);
		
		
		UI.register("Flaps down", 	func{me._switch.onAdjust(-1); } 	);
		UI.register("Flaps up",		func{me._switch.onAdjust(1); } 	);
		UI.register("Flaps 0", 		func{me._switch.onSet(1); } 	);
		UI.register("Flaps 15", 	func{me._switch.onSet(0); } 	);
		UI.register("Flaps 30", 	func{me._switch.onSet(-1); } );
				
	},
	_onDimTestChange : func(n){
		if (n.getValue() == 1){
			me._ledTrans.testOn();
			me._led15.testOn();
			me._led30.testOn();
		}else{
			me._ledTrans.testOff();
			me._led15.testOff();
			me._led30.testOff();
		}
	},
	_updateTransitionLight : func(n) {
		if (n.getValue() == 1){
			me._ledTrans.on();
		}elsif (n.getValue() == 0){
			me._ledTrans.off();
		}else{
			me._ledTrans.off();
		}		
	},
	_update15degLight : func(n) {
		if (n.getValue() == 1){
			me._led15.on();
		}elsif (n.getValue() == 0){
			me._led15.off();
		}else{
			me._led15.off();
		}		
	},
	_update30degLight : func(n) {
		if (n.getValue() == 1){
			me._led30.on();
		}elsif (n.getValue() == 0){
			me._led30.off();
		}else{
			me._led30.off();
		}		
	},
	_updateUNBCB : func(n) {
		if (n.getValue() == 0){
			setprop("/extra500/panel/CircuitBreaker/BankC/FlapUNB/state",0);
		}		
	}
};

var flapSystem = FlapSystemClass.new("extra500/system/flap","Flap Control");
